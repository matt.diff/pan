package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

/*
Prometheus Alertmanager Webhook payload
(see https://prometheus.io/docs/alerting/configuration/#webhook_config):
{
  "version": "4",
  "groupKey": <string>,    // key identifying the group of alerts (e.g. to deduplicate)
  "status": "<resolved|firing>",
  "receiver": <string>,
  "groupLabels": <object>,
  "commonLabels": <object>,
  "commonAnnotations": <object>,
  "externalURL": <string>,  // backlink to the Alertmanager.
  "alerts": [
    {
      "status": "<resolved|firing>",
      "labels": <object>,
      "annotations": <object>,
      "startsAt": "<rfc3339>",
      "endsAt": "<rfc3339>",
      "generatorURL": <string> // identifies the entity that caused the alert
    },
    ...
  ]
}

Asserted types for killMarathonTaskHandler interfaces:
&Alert.Labels      -> map[string]string
&Alert.Annotations -> map[string]string
*/

//Alert is the structure of an array element of the "alerts" key of the notification payload from
//Prometheus Alertmanager.  Payload is in JSON format
type Alert struct {
	Status       string      `json:"status"`
	Labels       interface{} `json:"labels"`
	Annotations  interface{} `json:"annotations"`
	StartsAt     string      `json:"startsAt"`
	EndsAt       string      `json:"endsAt"`
	GeneratorURL string      `json:"generatorURL"`
}

//AlertmanagerPayload is the structure of the notification sent by Prometheus Alertmanager to a receiver
//of type webhook.  Payload is in JSON format
type AlertmanagerPayload struct {
	Version           string      `json:"version"`
	GroupKey          string      `json:"groupKey"`
	Status            string      `json:"status"`
	Receiver          string      `json:"receiver"`
	GroupLabels       interface{} `json:"groupLabel"`
	CommonLabels      interface{} `json:"commonLabels"`
	CommonAnnotations interface{} `json:"commonAnnotations"`
	ExternalURL       string      `json:"externalURL"`
	Alerts            []Alert     `json:"alerts"`
}

func bufferRequest(req *http.Request) []byte {
	//pull request into a buffer
	buf, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	req.Body.Close()

	return buf
}

func killMarathonTaskHandler(w http.ResponseWriter, req *http.Request) {
	var task string

	//app is derived from the incoming JSON blob in the key
	//.AlertmanagerPayload.Alert.Annotations.summary
	//whose value must conform to the structure
	//'http://<marathon>:<port>/v2/apps/path/to/app'
	var app string
	var payload AlertmanagerPayload

	buf := bufferRequest(req)

	err := json.Unmarshal(buf, &payload)
	if err != nil {
		log.Print("Could not unmarshal JSON payload")
	}

	//isolate alerts array for processing
	alerts := payload.Alerts

	//extract task id from alert labels
	for _, a := range alerts {
		app, task = "", ""
		for k, v := range a.Labels.(map[string]interface{}) {
			if k == "taskid" && v != "" {
				task = v.(string)
			} else {
				log.Print("No task ID, skipping")
				continue
			}
		}

		//extract summary from alert annotations
		for k, v := range a.Annotations.(map[string]interface{}) {
			if k == "summary" && v != "" {
				app = v.(string)
			} else {
				log.Print("No app path, skipping")
				continue
			}
		}

		//task DELETE method requires multiple parameters
		//see Marathon API documentation for details on url composition:
		//https://docs.d2iq.com/mesosphere/dcos/2.0/deploying-services/marathon-api/#/default/DELETE_v2_apps_app_id_tasks
		url := app + "/tasks/" + task + "?force=true&scale=false&wipe=false"
		c := http.DefaultClient

		//set up HTTP request
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			log.Print(err)
		}

		//issue HTTP request
		resp, err := c.Do(req)
		if err != nil {
			log.Print(err)
		}

		if resp.Body != nil {
			defer resp.Body.Close()
		}

		log.Print(url + ", response from server: " + resp.Status)
	}

}

func main() {
	http.HandleFunc("/marathon/kill", killMarathonTaskHandler)
	http.Handle("/marathon", http.NotFoundHandler())

	log.Fatal(http.ListenAndServe(":8686", nil))
}
